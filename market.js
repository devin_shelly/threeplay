should_save_sort = true;
midnight_offset = 0;

//cookies.js
!function(e){var n=!1;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var o=window.Cookies,t=window.Cookies=e();t.noConflict=function(){return window.Cookies=o,t}}}(function(){function e(){for(var e=0,n={};e<arguments.length;e++){var o=arguments[e];for(var t in o)n[t]=o[t]}return n}function n(o){function t(n,r,i){var c;if("undefined"!=typeof document){if(arguments.length>1){if("number"==typeof(i=e({path:"/"},t.defaults,i)).expires){var a=new Date;a.setMilliseconds(a.getMilliseconds()+864e5*i.expires),i.expires=a}i.expires=i.expires?i.expires.toUTCString():"";try{c=JSON.stringify(r),/^[\{\[]/.test(c)&&(r=c)}catch(e){}r=o.write?o.write(r,n):encodeURIComponent(String(r)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),n=(n=(n=encodeURIComponent(String(n))).replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent)).replace(/[\(\)]/g,escape);var s="";for(var f in i)i[f]&&(s+="; "+f,!0!==i[f]&&(s+="="+i[f]));return document.cookie=n+"="+r+s}n||(c={});for(var p=document.cookie?document.cookie.split("; "):[],d=/(%[0-9A-Z]{2})+/g,u=0;u<p.length;u++){var l=p[u].split("="),C=l.slice(1).join("=");this.json||'"'!==C.charAt(0)||(C=C.slice(1,-1));try{var g=l[0].replace(d,decodeURIComponent);if(C=o.read?o.read(C,g):o(C,g)||C.replace(d,decodeURIComponent),this.json)try{C=JSON.parse(C)}catch(e){}if(n===g){c=C;break}n||(c[g]=C)}catch(e){}}return c}}return t.set=t,t.get=function(e){return t.call(t,e)},t.getJSON=function(){return t.apply({json:!0},[].slice.call(arguments))},t.defaults={},t.remove=function(n,o){t(n,"",e(o,{expires:-1}))},t.withConverter=n,t}return n(function(){})});
//# sourceMappingURL=/sm/f6937b1819ab68f00d8b787ead6c16bfb67977e0c408909621a3b2ff82dbad4a.map
 
//Calculating today's pay
 
offsetDate = function()
{
    //This starts a new day at midnight
    //Change the zero to whatever number you like if you work after midnight
    //For example, I use 6 because I never work past 6 AM, but sometimes I will work at 2 AM and want that to count for the previous day
   
    offset = midnight_offset * 1000 * 3600;
    return new Date(Date.now() - offset);
}
 
updatePay = function()
{
    console.log("Starting to update pay");
    totalS = $($("#current_pay h2")[0]).text();
    total = parseFloat(totalS.substr(1).replace(",", ""));
    console.log("The total pay is: " + total);
    if (isNaN(total))
    {
        return;
    }
    
    now = offsetDate();
    if (Cookies.get('current_date') != now.toDateString())
    {
        Cookies.set ('current_date', now.toDateString(), {expires: 1});
        Cookies.set('yesterdays_total', total, {expires: 1});
    }
    today = total - parseFloat(Cookies.get('yesterdays_total'));
    if (today < 0)
    {
        Cookies.set('yesterdays_total', 0, { expires: 1});
        today = total;
    }
    console.log("Yesterday's total is: " + Cookies.get('yesterdays_total'));
    console.log("Today's pay is: " + today);
    
    $("#current_pay").append('<h2 class = "muted" style = "margin_top: 10px"> $' + today.toFixed(2) + "</h2>");
    $("#current_pay").append('<div class = "secondColor">TODAY</div>');
    $("#footer_nav ul.box_style li").css("height", 210);
    
    console.log($("#current_pay").html());
    console.log("Stopped updating pay");
}
 
updatePay();

add_year_to_date = function()
{
  earnings = eval($(".main script")[0].text.split("var data = ")[1].split(";")[0])[0];
  months = $(".tickLabel");
  end = months[11].textContent.split(" ")[1];
  ytd_earnings = earnings[11][1];
  for (var i = 10; i>=0; i--)
  {
    console.log("The month is: " + months[i].textContent);
	console.log("The earnings are: " + earnings[i][1]);
    end_i = months[i].textContent.split(" ")[1];
    if (end === end_i)
    {
      ytd_earnings += earnings[i][1];
    }
  }
  
  console.log("Your earnings are:" + ytd_earnings);
  if ($("#year_to_date").length == 0)
  {
    $("h1").after("<br><h5 id = 'year_to_date'>Year to Date</h5><h1>$" + ytd_earnings.toLocaleString() + "</h1>");
	clearInterval(add_interval_id);
  }
}

if (window.location.href === "https://jobs.3playmedia.com/pay_stubs")
{
  add_interval_id = setInterval(add_year_to_date, 100);
}

save_sort = function(e, val = null)
{
  Cookies.set("sort_by", val || $("#sort_by").val());
}

setInterval(function(){$(".clickable_row td").filter(function(){return $(this).index() <9}).removeClass("no-click no_follow")}, 200);

if (document.URL.startsWith("https://jobs.3playmedia.com/assigned_jobs"))
{
  $("#sort_by").change(save_sort);
  if(Cookies.get("sort_by") && should_save_sort)
  {
    $("#sort_by").val(Cookies.get("sort_by")).trigger("change");
  }
  else
  {
    save_sort(null, "Deadline (earliest first)");
  }
}